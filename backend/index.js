const express = require('express');
const cors = require('cors')
const mongoose = require('mongoose');
require('dotenv').config();

const routes = require('./routes/routes');

const mongoString = process.env.DATABASE_URL

const app = express();
app.use(express.json());
app.use(cors())
app.use('/v1', routes)

mongoose.connect(mongoString);
const database = mongoose.connection;


database.on('error', (error) => {
  console.log(error)
})

database.once('connected', () => {
  console.log('Database Connected');
})

app.listen(3333, () => {
  console.log(`Server Started at ${3333}`)
})