const getABI = contract => {
  const ABI = contract.abi;
  const address = contract.networks[5777].address;     // 5777 is the name/number of the test chain. To modify when moving from dev environment
  const data = { ABI, address };
  return data
}

module.exports = { getABI }