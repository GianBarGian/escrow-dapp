import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  isInstalled: false,
  isConnected: false,
  isProvider: false,
  account: ''
}

export const metamaskSlice = createSlice({
  name: 'metamask',
  initialState,
  reducers: {
    connectMetamask: (state, action) => {
      state.account = action.payload
    },
    setIsInstalled: (state, action) => {
      state.isInstalled = action.payload
    },
    setIsProvider: (state, action) => {
      state.isProvider = action.payload
    },
    setIsConnected: (state, action) => {
      state.isConnected = action.payload
    },
  }
})

export const { connectMetamask, setIsInstalled, setIsProvider, setIsConnected } = metamaskSlice.actions

export default metamaskSlice.reducer