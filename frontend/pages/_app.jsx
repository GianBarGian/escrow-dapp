import Layout from "../components/layout";
import "../styles/globals.css";
import { wrapper } from "../store/store";

const App = ({ Component, pageProps }) => {
  return (
    <Layout>
      <Component {...pageProps} />;
    </Layout>
  );
};

export default wrapper.withRedux(App);
