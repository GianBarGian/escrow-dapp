import React, { useEffect } from "react";
import Metamask from "./metamask";

const Layout = (props) => {
  const { children } = props;

  return (
    <>
      <main>
        <Metamask />
        {children}
      </main>
    </>
  );
};

export default Layout;
